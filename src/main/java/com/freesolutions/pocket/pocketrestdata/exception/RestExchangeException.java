package com.freesolutions.pocket.pocketrestdata.exception;

import com.freesolutions.pocket.pocketrestdata.dto.IResponse;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @author Stanislau Mirzayeu
 */
public class RestExchangeException extends RuntimeException {

    @Nullable
    public final Integer statusCode;
    @Nullable
    public final Object response;
    @Nullable
    public final String description;

    @Nonnull
    public static RestExchangeException of() {
        return new RestExchangeException(null, null);
    }

    @Nonnull
    public static RestExchangeException ofMessage(@Nonnull String message) {
        return new RestExchangeException(null, null, message);
    }

    @Nonnull
    public static RestExchangeException ofCause(@Nonnull Throwable cause) {
        return new RestExchangeException(null, null, cause);
    }

    @Nonnull
    public static RestExchangeException ofCauseWithMessage(@Nonnull String message, @Nonnull Throwable cause) {
        return new RestExchangeException(null, null, message, cause);
    }

    @Nonnull
    public static RestExchangeException ofResult(int statusCode, @Nullable Object response) {
        return new RestExchangeException(statusCode, response);
    }

    @Nonnull
    public static RestExchangeException ofResultWithMessage(int statusCode, @Nullable Object response, @Nonnull String message) {
        return new RestExchangeException(statusCode, response, message);
    }

    //useful method
    @Nonnull
    public <T> IResponse<T> iResponse() {
        return IResponse.ofObject(response);
    }

    private RestExchangeException(
        @Nullable Integer statusCode,
        @Nullable Object response
    ) {
        super(enrichMessage(null, statusCode, response));
        this.statusCode = statusCode;
        this.response = response;
        this.description = toDescription(response);
    }

    private RestExchangeException(
        @Nullable Integer statusCode,
        @Nullable Object response,
        @Nonnull Throwable cause
    ) {
        super(enrichMessage(null, statusCode, response), cause);
        this.statusCode = statusCode;
        this.response = response;
        this.description = toDescription(response);
    }

    private RestExchangeException(
        @Nullable Integer statusCode,
        @Nullable Object response,
        @Nonnull String message
    ) {
        super(enrichMessage(message, statusCode, response));
        this.statusCode = statusCode;
        this.response = response;
        this.description = toDescription(response);
    }

    private RestExchangeException(
        @Nullable Integer statusCode,
        @Nullable Object response,
        @Nonnull String message,
        @Nonnull Throwable cause
    ) {
        super(enrichMessage(message, statusCode, response), cause);
        this.statusCode = statusCode;
        this.response = response;
        this.description = toDescription(response);
    }

    @Nonnull
    private static String enrichMessage(@Nullable String message, @Nullable Integer statusCode, @Nullable Object response) {
        return (message != null && !message.isEmpty() ? message + " " : "") + "(statusCode: " + statusCode + ", description -> " + toDescription(response) + ")";
    }

    @Nullable
    private static String toDescription(@Nullable Object response) {
        return IResponse.descriptionOf(response);
    }
}
