package com.freesolutions.pocket.pocketrestdata.exception;

import com.freesolutions.pocket.pocketrestdata.dto.ApiResponse;
import com.freesolutions.pocket.pocketrestdata.dto.Reason;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @author Stanislau Mirzayeu
 */
public class ResponseException extends RuntimeException {

    @Nullable
    public final Reason reason;
    @Nullable
    public final ApiResponse<?> response;
    @Nullable
    public final Object statusCode;

    public ResponseException() {
        super();
        this.reason = null;
        this.response = null;
        this.statusCode = null;
    }

    public ResponseException(@Nullable Reason reason) {
        super();
        this.reason = reason;
        this.response = null;
        this.statusCode = null;
    }

    public ResponseException(@Nullable ApiResponse<?> response) {
        super();
        this.reason = null;
        this.response = response;
        this.statusCode = null;
    }

    public ResponseException(@Nullable Object statusCode) {
        super();
        this.reason = null;
        this.response = null;
        this.statusCode = statusCode;
    }

    public ResponseException(@Nullable Reason reason, @Nullable Object statusCode) {
        super();
        this.reason = reason;
        this.response = null;
        this.statusCode = statusCode;
    }

    public ResponseException(@Nullable ApiResponse<?> response, @Nullable Object statusCode) {
        super();
        this.reason = null;
        this.response = response;
        this.statusCode = statusCode;
    }

    public ResponseException(@Nonnull String message) {
        super(message);
        this.reason = null;
        this.response = null;
        this.statusCode = null;
    }

    public ResponseException(@Nonnull String message, @Nullable Reason reason) {
        super(message);
        this.reason = reason;
        this.response = null;
        this.statusCode = null;
    }

    public ResponseException(@Nonnull String message, @Nullable ApiResponse<?> response) {
        super(message);
        this.reason = null;
        this.response = response;
        this.statusCode = null;
    }

    public ResponseException(@Nonnull String message, @Nullable Object statusCode) {
        super(message);
        this.reason = null;
        this.response = null;
        this.statusCode = statusCode;
    }

    public ResponseException(@Nonnull String message, @Nullable Reason reason, @Nullable Object statusCode) {
        super(message);
        this.reason = reason;
        this.response = null;
        this.statusCode = statusCode;
    }

    public ResponseException(@Nonnull String message, @Nullable ApiResponse<?> response, @Nullable Object statusCode) {
        super(message);
        this.reason = null;
        this.response = response;
        this.statusCode = statusCode;
    }

    public ResponseException(@Nonnull String message, @Nonnull Throwable cause) {
        super(message, cause);
        this.reason = null;
        this.response = null;
        this.statusCode = null;
    }

    public ResponseException(@Nonnull String message, @Nullable Reason reason, @Nonnull Throwable cause) {
        super(message, cause);
        this.reason = reason;
        this.response = null;
        this.statusCode = null;
    }

    public ResponseException(@Nonnull String message, @Nullable ApiResponse<?> response, @Nonnull Throwable cause) {
        super(message, cause);
        this.reason = null;
        this.response = response;
        this.statusCode = null;
    }

    public ResponseException(@Nonnull String message, @Nullable Object statusCode, @Nonnull Throwable cause) {
        super(message, cause);
        this.reason = null;
        this.response = null;
        this.statusCode = statusCode;
    }

    public ResponseException(@Nonnull String message, @Nullable Reason reason, @Nullable Object statusCode, @Nonnull Throwable cause) {
        super(message, cause);
        this.reason = reason;
        this.response = null;
        this.statusCode = statusCode;
    }

    public ResponseException(@Nonnull String message, @Nullable ApiResponse<?> response, @Nullable Object statusCode, @Nonnull Throwable cause) {
        super(message, cause);
        this.reason = null;
        this.response = response;
        this.statusCode = statusCode;
    }

    public ResponseException(@Nonnull Throwable cause) {
        super(cause);
        this.reason = null;
        this.response = null;
        this.statusCode = null;
    }

    public ResponseException(@Nullable Reason reason, @Nonnull Throwable cause) {
        super(cause);
        this.reason = reason;
        this.response = null;
        this.statusCode = null;
    }

    public ResponseException(@Nullable ApiResponse<?> response, @Nonnull Throwable cause) {
        super(cause);
        this.reason = null;
        this.response = response;
        this.statusCode = null;
    }

    public ResponseException(@Nullable Object statusCode, @Nonnull Throwable cause) {
        super(cause);
        this.reason = null;
        this.response = null;
        this.statusCode = statusCode;
    }

    public ResponseException(@Nullable Reason reason, @Nullable Object statusCode, @Nonnull Throwable cause) {
        super(cause);
        this.reason = reason;
        this.response = null;
        this.statusCode = statusCode;
    }

    public ResponseException(@Nullable ApiResponse<?> response, @Nullable Object statusCode, @Nonnull Throwable cause) {
        super(cause);
        this.reason = null;
        this.response = response;
        this.statusCode = statusCode;
    }
}
