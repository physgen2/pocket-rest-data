package com.freesolutions.pocket.pocketrestdata.utils;

import javax.annotation.Nonnull;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * @author Stanislau Mirzayeu
 */
public class UriEncoder {

    private static final Charset CHARSET = StandardCharsets.UTF_8;

    private static final byte[] HEX = {
        (byte) '0', (byte) '1', (byte) '2', (byte) '3', (byte) '4', (byte) '5', (byte) '6', (byte) '7',
        (byte) '8', (byte) '9', (byte) 'A', (byte) 'B', (byte) 'C', (byte) 'D', (byte) 'E', (byte) 'F'
    };

    public static boolean isAllowed(int ch) {
        return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || (ch >= '0' && ch <= '9') || ch == '-' || ch == '.' || ch == '_' || ch == '~';
    }

    @Nonnull
    public static String encode(@Nonnull String value) {
        if (value.isEmpty()) { //short case
            return value;
        }
        byte[] bytes = value.getBytes(CHARSET);
        byte[] r = null;
        int ri = 0;
        for (int b : bytes) {
            b &= 0x000000ff;
            if (isAllowed(b)) {
                if (r != null) {
                    r[ri++] = (byte) b;
                } else {
                    ri++;
                }
            } else {
                if (r == null) {
                    r = new byte[3 * bytes.length];//max possible size
                    System.arraycopy(bytes, 0, r, 0, ri);
                }
                r[ri++] = (byte) '%';
                r[ri++] = HEX[(b >> 4) & 0x0000000f];
                r[ri++] = HEX[b & 0x0000000f];
            }
        }
        return r != null ? new String(r, 0, ri, CHARSET) : value;
    }

    @Nonnull
    public static String decode(@Nonnull String value) {
        if (value.isEmpty()) { //short case
            return value;
        }
        byte[] bytes = value.getBytes(CHARSET);
        byte[] r = null;
        int ri = 0;
        int l = bytes.length;
        for (int bi = 0; bi < l; ) {
            int b = (int) bytes[bi++] & 0x000000ff;
            if (bi + 2 > l || b != '%') {
                //pass
                if (r != null) {
                    r[ri++] = (byte) b;
                } else {
                    ri++;
                }
            } else {
                int h1 = (int) bytes[bi++] & 0x000000ff;
                int h2 = (int) bytes[bi++] & 0x000000ff;
                int d1 = fromHex(h1);
                int d2 = fromHex(h2);
                if (d1 < 0 || d2 < 0) {
                    //error => just pass chars as is
                    if (r != null) {
                        r[ri++] = (byte) b;
                        r[ri++] = (byte) h1;
                        r[ri++] = (byte) h2;
                    } else {
                        ri += 3;
                    }
                } else {
                    //collapse (d1, d2) into decoded char
                    if (r == null) {
                        r = new byte[l];//max possible size
                        System.arraycopy(bytes, 0, r, 0, ri);
                    }
                    r[ri++] = (byte) ((d1 << 4) | d2);
                }
            }
        }
        return r != null ? new String(r, 0, ri, CHARSET) : value;
    }

    private static int fromHex(int h) {
        if (h >= '0' && h <= '9') {
            return h - '0';
        }
        if (h >= 'A' && h <= 'F') {
            return 10 + (h - 'A');
        }
        if (h >= 'a' && h <= 'f') {
            return 10 + (h - 'a');
        }
        return -1;
    }
}
