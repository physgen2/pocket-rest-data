package com.freesolutions.pocket.pocketrestdata.utils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @author Stanislau Mirzayeu
 */
public class Utils {

    @Nonnull
    public static <T> T checkNotNull(@Nullable T value) {
        if (value == null) {
            throw new NullPointerException();
        }
        return value;
    }

    @Nonnull
    public static <T> T checkNotNull(@Nullable T value, @Nonnull String message) {
        if (value == null) {
            throw new NullPointerException(message);
        }
        return value;
    }

    @Nonnull
    public static <T> T checkNotNull(@Nullable T value, @Nonnull String message, @Nonnull Object... args) {
        if (value == null) {
            throw new NullPointerException(String.format(message, args));
        }
        return value;
    }

    public static void checkArgument(boolean condition) {
        if (!condition) {
            throw new IllegalArgumentException();
        }
    }

    public static void checkArgument(boolean condition, @Nonnull String message) {
        if (!condition) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void checkArgument(boolean condition, @Nonnull String message, @Nonnull Object... args) {
        if (!condition) {
            throw new IllegalArgumentException(String.format(message, args));
        }
    }

    public static void checkState(boolean state) {
        if (!state) {
            throw new IllegalStateException();
        }
    }

    public static void checkState(boolean state, @Nonnull String message) {
        if (!state) {
            throw new IllegalStateException(message);
        }
    }

    public static void checkState(boolean state, @Nonnull String message, @Nonnull Object... args) {
        if (!state) {
            throw new IllegalStateException(String.format(message, args));
        }
    }
}
