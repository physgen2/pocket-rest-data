package com.freesolutions.pocket.pocketrestdata.dto.converter;

import com.fasterxml.jackson.databind.util.StdConverter;

import javax.annotation.Nullable;

/**
 * @author Stanislau Mirzayeu
 */
public class StringToLongConverter extends StdConverter<String, Long> {

    @Nullable
    @Override
    public Long convert(@Nullable String value) {
        return value != null ? Long.parseLong(value) : null;
    }
}
