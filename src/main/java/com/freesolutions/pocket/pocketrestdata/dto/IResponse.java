package com.freesolutions.pocket.pocketrestdata.dto;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public interface IResponse<T> {

    IResponse<?> EMPTY = new IResponse<>() {
        @Nullable
        @Override
        public Object result() {
            return null;
        }

        @Nullable
        @Override
        public String description() {
            return null;
        }
    };

    static <T> IResponse<T> ofObject(@Nullable Object obj) {

        if (obj == null) {
            @SuppressWarnings("unchecked")
            IResponse<T> response = (IResponse<T>) EMPTY;
            return response;
        }

        if (obj instanceof IResponse) {
            @SuppressWarnings("unchecked")
            IResponse<T> response = (IResponse<T>) obj;
            return response;
        }

        @SuppressWarnings("unchecked")
        T result = (T) obj;//object itself is result, ok

        return ofResult(result);
    }

    static <T> IResponse<T> ofResult(@Nullable T result) {
        return ofResultWithDescription(result, null);
    }

    static <T> IResponse<T> ofResultWithDescription(@Nullable T result, @Nullable String description) {
        return new IResponse<T>() {
            @Nullable
            @Override
            public T result() {
                return result;
            }

            @Nullable
            @Override
            public String description() {
                return description;
            }
        };
    }

    @Nullable
    static <T> T resultOf(@Nullable Object obj) {
        if (obj instanceof IResponse) {
            @SuppressWarnings("unchecked")
            IResponse<T> response = (IResponse<T>) obj;
            return response.result();
        }
        @SuppressWarnings("unchecked")
        T result = (T) obj;//object itself is result, ok
        return result;
    }

    @Nullable
    static String descriptionOf(@Nullable Object obj) {
        if (obj instanceof IResponse) {
            return ((IResponse<?>) obj).description();
        }
        return null;
    }

    @Nullable
    T result();

    @Nullable
    String description();
}
