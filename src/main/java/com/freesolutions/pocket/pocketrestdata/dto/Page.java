package com.freesolutions.pocket.pocketrestdata.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.List;

import static com.freesolutions.pocket.pocketrestdata.utils.Utils.checkArgument;
import static com.freesolutions.pocket.pocketrestdata.utils.Utils.checkState;

/**
 * @author Stanislau Mirzayeu
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Immutable
public class Page {

    @Immutable
    public static class PageQuery {

        public static final int DEFAULT_PAGE_OFFSET = 0;
        public static final int DEFAULT_PAGE_NUMBER = 1;

        public final int pageOffset;//counting from 0
        public final int pageNumber;//counting from 1
        public final int pageSize;

        @Nonnull
        public static PageQuery ofPageOffsetOrDefault(@Nullable Integer pageOffset, int pageSize) {
            return pageOffset != null ?
                Page.PageQuery.ofPageOffset(pageOffset, pageSize) :
                Page.PageQuery.ofDefault(pageSize);
        }

        @Nonnull
        public static PageQuery ofDefault(int pageSize) {
            checkArgument(
                pageSize > 0,
                "pageSize must be positive, actual pageSize: %s",
                pageSize
            );
            return new PageQuery(DEFAULT_PAGE_OFFSET, DEFAULT_PAGE_NUMBER, pageSize);
        }

        @Nonnull
        public static PageQuery ofPageOffset(int pageOffset, int pageSize) {
            checkArgument(
                pageOffset >= 0,
                "pageOffset must be not negative, actual pageOffset: %s",
                pageOffset
            );
            checkArgument(
                pageSize > 0,
                "pageSize must be positive, actual pageSize: %s",
                pageSize
            );
            int pageNumber = (pageOffset > 0 ? ((pageOffset - 1) / pageSize) + 1 : 0) + 1;
            return new PageQuery(pageOffset, pageNumber, pageSize);
        }

        @Nonnull
        public static PageQuery ofPageNumber(int pageNumber, int pageSize) {
            checkArgument(
                pageNumber > 0,
                "pageNumber must be positive, actual pageNumber: %s",
                pageNumber
            );
            checkArgument(
                pageSize > 0,
                "pageSize must be positive, actual pageSize: %s",
                pageSize
            );
            return new PageQuery((pageNumber - 1) * pageSize, pageNumber, pageSize);
        }

        public boolean pageExists(int total) {
            checkArgument(
                total >= 0,
                "total must be not negative, actual: %s",
                total
            );
            return total > 0 && pageOffset < total;
        }

        public int pages(int total) {
            checkArgument(
                total >= 0,
                "total must be not negative, actual: %s",
                total
            );
            int delta = total - pageOffset;
            return pageNumber - 1 + (delta > 0 ? (delta - 1) / pageSize + 1 : delta / pageSize);
        }

        public void checkDataSize(int dataSize) {
            checkArgument(
                dataSize <= pageSize,
                "data of simple page must fit into page, actual data.size: %s, pageSize: %s",
                dataSize, pageSize
            );
        }

        @Nonnull
        public <T> List<T> pageDataSublist(@Nonnull List<T> fullData) {
            int from = Math.min(pageOffset, fullData.size());
            int to = Math.min(pageOffset + pageSize, fullData.size());
            return fullData.subList(from, to);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof PageQuery)) return false;
            PageQuery pageQuery = (PageQuery) o;
            if (pageOffset != pageQuery.pageOffset) return false;
            if (pageNumber != pageQuery.pageNumber) return false;
            return pageSize == pageQuery.pageSize;
        }

        @Override
        public int hashCode() {
            int result = pageOffset;
            result = 31 * result + pageNumber;
            result = 31 * result + pageSize;
            return result;
        }

        @Override
        public String toString() {
            return "PageQuery[" +
                "pageOffset: " + pageOffset +
                ", pageNumber: " + pageNumber +
                ", pageSize: " + pageSize +
                "]";
        }

        private PageQuery(int pageOffset, int pageNumber, int pageSize) {
            this.pageOffset = pageOffset;
            this.pageNumber = pageNumber;
            this.pageSize = pageSize;
        }
    }

    @Immutable
    public static class PageInfo {

        @Nonnull
        public final PageQuery query;
        public final boolean exists;
        public final boolean more;
        public final int pages;
        public final int total;

        @Nonnull
        public static PageInfo ofEmptyList(int pageSize) {
            return of(PageQuery.ofDefault(pageSize), 0);
        }

        @Nonnull
        public static PageInfo of(@Nonnull PageQuery query, int total) {
            int pages = query.pages(total);//have check inside, ok
            return new PageInfo(
                query,
                query.pageExists(total),//have check inside, ok
                query.pageNumber < pages,//nonexistent pages with number < 0 will also showed as "more: true", this is ok.
                pages,
                total
            );
        }

        public boolean isEmptyList() {
            return total == 0;
        }

        public int dataSize() {
            return exists ?
                (more ? query.pageSize : total - query.pageOffset) :
                0;
        }

        public int offsetTo() {
            return exists ?
                (more ? query.pageOffset + query.pageSize : total) :
                query.pageOffset;
        }

        public void checkDataSize(int dataSize) {
            if (!exists) {
                checkArgument(
                    dataSize == 0,
                    "no data must be present for non-existing page, actual dataSize: %s",
                    dataSize
                );
            } else {
                checkState(total > 0);
                if (more) {
                    checkArgument(
                        dataSize == query.pageSize,
                        "not last page expected to be completely filled, actual data.size: %s, pageSize: %s",
                        dataSize, query.pageSize
                    );
                } else {
                    checkArgument(
                        dataSize > 0,
                        "at least one data item must be present on last page, actual dataSize: %s",
                        dataSize
                    );
                    int pageOffset = query.pageOffset;
                    checkArgument(
                        pageOffset + dataSize == total,
                        "amount of data on last page must correspond remaining part, actual pageOffset: %s, data.size: %s, total: %s",
                        pageOffset, dataSize, total
                    );
                }
            }
        }

        @Nonnull
        public <T> List<T> pageData(@Nonnull List<T> allData) {
            int allDataSize = allData.size();
            checkArgument(
                allDataSize == total,
                "all data size expected to be equal to total, actual allData.size: %s, total: %s",
                allDataSize, total
            );
            return exists ? allData.subList(query.pageOffset, offsetTo()) : Collections.emptyList();
        }

        @Nonnull
        public Page toNonexistentPage() {
            return Page.ofNonexistentPage(this);
        }

        @Nonnull
        public <T> Page toPage(@Nonnull List<T> pageData) {
            return Page.ofPage(this, pageData);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof PageInfo)) return false;
            PageInfo pageInfo = (PageInfo) o;
            if (exists != pageInfo.exists) return false;
            if (more != pageInfo.more) return false;
            if (pages != pageInfo.pages) return false;
            if (total != pageInfo.total) return false;
            return query.equals(pageInfo.query);
        }

        @Override
        public int hashCode() {
            int result = query.hashCode();
            result = 31 * result + (exists ? 1 : 0);
            result = 31 * result + (more ? 1 : 0);
            result = 31 * result + pages;
            result = 31 * result + total;
            return result;
        }

        @Override
        public String toString() {
            return "PageInfo[" +
                "query: " + query +
                ", exists: " + exists +
                ", more: " + more +
                ", pages: " + pages +
                ", total: " + total +
                "]";
        }

        private PageInfo(
            @Nonnull PageQuery query,
            boolean exists,
            boolean more,
            int pages,
            int total
        ) {
            this.query = query;
            this.exists = exists;
            this.more = more;
            this.pages = pages;
            this.total = total;
        }
    }

    @JsonProperty("offset")
    public final int offset;

    @JsonProperty("number")
    public final int number;

    @JsonProperty("size")
    public final int size;

    @JsonProperty("pageExists")
    @Nullable
    public final Boolean pageExists;

    @JsonProperty("more")
    @Nullable
    public final Boolean more;

    @JsonProperty("pages")
    @Nullable
    public final Integer pages;

    @JsonProperty("total")
    @Nullable
    public final Integer total;

    /**
     * Nonexistent page.
     */
    @Nonnull
    public static Page ofNonexistentPage(@Nonnull PageInfo pageInfo) {
        checkArgument(
            !pageInfo.exists,
            "page expected to be nonexistent, actual: %s",
            pageInfo
        );
        pageInfo.checkDataSize(0);
        return new Page(
            pageInfo.query.pageOffset,
            pageInfo.query.pageNumber,
            pageInfo.query.pageSize,
            pageInfo.exists,
            pageInfo.more,
            pageInfo.pages,
            pageInfo.total
        );
    }

    /**
     * Page by non-empty page data.
     */
    @Nonnull
    public static <T> Page ofPage(@Nonnull PageInfo pageInfo, @Nonnull List<T> pageData) {
        pageInfo.checkDataSize(pageData.size());
        return new Page(
            pageInfo.query.pageOffset,
            pageInfo.query.pageNumber,
            pageInfo.query.pageSize,
            pageInfo.exists,
            pageInfo.more,
            pageInfo.pages,
            pageInfo.total
        );
    }

    /**
     * Empty simple page (without full information)
     */
    @Nonnull
    public static Page ofEmptySimplePage(@Nonnull PageQuery pageQuery) {
        return new Page(
            pageQuery.pageOffset,
            pageQuery.pageNumber,
            pageQuery.pageSize,
            null,
            null,
            null,
            null
        );
    }

    /**
     * Simple page (without full information) by non-empty page data.
     */
    @Nonnull
    public static <T> Page ofSimplePage(@Nonnull PageQuery pageQuery, @Nonnull List<T> pageData) {
        pageQuery.checkDataSize(pageData.size());
        return new Page(
            pageQuery.pageOffset,
            pageQuery.pageNumber,
            pageQuery.pageSize,
            null,
            null,
            null,
            null
        );
    }

    @JsonCreator
    public Page(
        @JsonProperty("offset") int offset,
        @JsonProperty("number") int number,
        @JsonProperty("size") int size,
        @JsonProperty("pageExists") @Nullable Boolean pageExists,
        @JsonProperty("more") @Nullable Boolean more,
        @JsonProperty("pages") @Nullable Integer pages,
        @JsonProperty("total") @Nullable Integer total
    ) {
        this.offset = offset;
        this.number = number;
        this.size = size;
        this.pageExists = pageExists;
        this.more = more;
        this.pages = pages;
        this.total = total;
    }

    @Override
    public String toString() {
        return "{" +
            "\"offset\": " + offset +
            ", \"number\": " + number +
            ", \"size\": " + size +
            (pageExists != null ? ", \"pageExists\": " + pageExists : "") +
            (more != null ? ", \"more\": " + more : "") +
            (pages != null ? ", \"pages\": " + pages : "") +
            (total != null ? ", \"total\": " + total : "") +
            "}";
    }
}
