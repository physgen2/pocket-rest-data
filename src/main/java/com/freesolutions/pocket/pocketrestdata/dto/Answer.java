package com.freesolutions.pocket.pocketrestdata.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.freesolutions.pocket.pocketrestdata.exception.ResponseException;
import com.freesolutions.pocket.pocketrestdata.value.AnswerCode;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.List;

import static com.freesolutions.pocket.pocketrestdata.utils.Utils.checkArgument;
import static com.freesolutions.pocket.pocketrestdata.utils.Utils.checkNotNull;

/**
 * @author Stanislau Mirzayeu
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Immutable
public class Answer<T> {

    private static final Integer BAD_REQUEST = 400;
    private static final Integer INTERNAL_SERVER_ERROR = 500;

    @JsonProperty("code")
    @Nonnull
    public final AnswerCode code;

    @JsonInclude //override common non-null-inclusion behavior
    @JsonProperty("result")
    @Nullable
    public final T result;

    @JsonProperty("reasons")
    @Nullable
    public final List<Reason> reasons;

    @JsonProperty("description")
    @Nullable
    public final String description;

    @Nonnull
    public static <T> Answer<T> absent() {
        return new Answer<>(AnswerCode.OK, null, null, null);
    }

    @Nonnull
    public static <T> Answer<T> success(@Nonnull T result) {
        checkNotNull(result, "must be !null");
        return new Answer<>(AnswerCode.OK, result, null, null);
    }

    @Nonnull
    public static <T> Answer<T> failure(@Nonnull AnswerCode code) {
        checkArgument(!code.ok, "must be !ok");
        return new Answer<>(code, null, null, null);
    }

    @Nonnull
    public static <T> Answer<T> failure(@Nonnull AnswerCode code, @Nullable Reason reason) {
        checkArgument(!code.ok, "must be !ok");
        return new Answer<>(code, null, reason != null ? Collections.singletonList(reason) : null, null);
    }

    @Nonnull
    public static <T> Answer<T> failure(@Nonnull AnswerCode code, @Nullable List<Reason> reasons) {
        checkArgument(!code.ok, "must be !ok");
        return new Answer<>(code, null, (reasons != null && !reasons.isEmpty()) ? reasons : null, null);
    }

    @Nonnull
    public static <T> Answer<T> failure(@Nonnull AnswerCode code, @Nullable String description) {
        checkArgument(!code.ok, "must be !ok");
        return new Answer<>(code, null, null, description);
    }

    @Nonnull
    public static <T> Answer<T> failure(@Nonnull AnswerCode code, @Nullable Reason reason, @Nullable String description) {
        checkArgument(!code.ok, "must be !ok");
        return new Answer<>(code, null, reason != null ? Collections.singletonList(reason) : null, description);
    }

    @Nonnull
    public static <T> Answer<T> failure(@Nonnull AnswerCode code, @Nullable List<Reason> reasons, @Nullable String description) {
        checkArgument(!code.ok, "must be !ok");
        return new Answer<>(code, null, (reasons != null && !reasons.isEmpty()) ? reasons : null, description);
    }

    @Nonnull
    public static <T> Answer<T> full(@Nonnull AnswerCode code, @Nullable T result, @Nullable Reason reason, @Nullable String description) {
        return new Answer<>(code, result, reason != null ? Collections.singletonList(reason) : null, description);
    }

    @Nonnull
    public static <T> Answer<T> full(@Nonnull AnswerCode code, @Nullable T result, @Nullable List<Reason> reasons, @Nullable String description) {
        return new Answer<>(code, result, (reasons != null && !reasons.isEmpty()) ? reasons : null, description);
    }

    @JsonCreator
    public Answer(
        @JsonProperty("code") @Nonnull AnswerCode code,
        @JsonProperty("result") @Nullable T result,
        @JsonProperty("reasons") @Nullable List<Reason> reasons,
        @JsonProperty("description") @Nullable String description
    ) {
        this.code = code;
        this.result = result;
        this.reasons = reasons;
        this.description = description;
    }

    @JsonIgnore
    public void check(@Nullable Reason reason) throws ResponseException {
        if (!code.ok) {
            if (reason == null && this.reasons != null && !this.reasons.isEmpty()) {
                reason = this.reasons.get(0);
            }
            throw new ResponseException(
                "Bad answer (code: " + code + ", result: " + result + ", reason: " + reason + ", description: " + description + ")",
                reason,
                code.rejected ?
                    BAD_REQUEST :
                    code.error ?
                        INTERNAL_SERVER_ERROR :
                        null
            );
        }
    }

    @Override
    public String toString() {
        return "{" +
            "\"code\": " + code +
            ", \"result\": " + result + //nullable included
            (reasons != null ? ", \"reasons\": " + reasons : "") +
            (description != null ? ", \"description\": " + description : "") +
            "}";
    }
}
