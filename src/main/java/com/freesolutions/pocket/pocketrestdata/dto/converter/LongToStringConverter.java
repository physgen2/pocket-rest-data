package com.freesolutions.pocket.pocketrestdata.dto.converter;

import com.fasterxml.jackson.databind.util.StdConverter;

import javax.annotation.Nullable;

/**
 * @author Stanislau Mirzayeu
 */
public class LongToStringConverter extends StdConverter<Long, String> {

    @Nullable
    @Override
    public String convert(@Nullable Long value) {
        return value != null ? value.toString() : null;
    }
}
