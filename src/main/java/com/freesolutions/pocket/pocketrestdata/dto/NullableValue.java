package com.freesolutions.pocket.pocketrestdata.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.Optional;

/**
 * @author Stanislau Mirzayeu
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Immutable
public class NullableValue<T> {

    @JsonInclude //override common non-null-inclusion behavior
    @JsonProperty("value")
    @Nullable
    public final T value;

    @Nonnull
    public static <T> NullableValue<T> empty() {
        return new NullableValue<>(null);
    }

    @Nonnull
    public static <T> NullableValue<T> of(@Nullable T value) {
        return new NullableValue<>(value);
    }

    @Nonnull
    public static <T> NullableValue<T> ofOptional(@Nonnull Optional<? extends T> optValue) {
        return new NullableValue<>(optValue.orElse(null));
    }

    @JsonCreator
    public NullableValue(
        @JsonProperty("value") @Nullable T value
    ) {
        this.value = value;
    }

    @JsonIgnore
    @Nonnull
    public Optional<T> toOptional() {
        return Optional.ofNullable(value);
    }

    @Override
    public String toString() {
        return "{" +
            "\"value\": " + value + //nullable included
            "}";
    }
}
