package com.freesolutions.pocket.pocketrestdata.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.freesolutions.pocket.pocketrestdata.value.KeyAware;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.List;

import static com.freesolutions.pocket.pocketrestdata.utils.Utils.checkNotNull;

/**
 * @author Stanislau Mirzayeu
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Immutable
public class ApiResponse<T> implements IResponse<T> {

    @JsonProperty("page")
    @Nullable
    public final Page page;

    @JsonInclude //override common non-null-inclusion behavior
    @JsonProperty("result")
    @Nullable
    public final T result;

    @JsonProperty("reasons")
    @Nullable
    public final List<Reason> reasons;

    @JsonProperty("description")
    @Nullable
    public final String description;

    @Nonnull
    public static <T> ApiResponse<T> absent() {
        return new ApiResponse<>(null, null, null, null);
    }

    @Nonnull
    public static <T> ApiResponse<T> result(@Nonnull T result) {
        checkNotNull(result, "result must be !null");
        return new ApiResponse<>(null, result, null, null);
    }

    @Nonnull
    public static <T> ApiResponse<T> resultOrAbsent(@Nullable T result) {
        return new ApiResponse<>(null, result, null, null);
    }

    @Nonnull
    public static <ITEM> ApiResponse<List<ITEM>> emptyList() {
        return new ApiResponse<>(null, Collections.emptyList(), null, null);
    }

    @Nonnull
    public static <ITEM> ApiResponse<List<ITEM>> singletonList(@Nonnull ITEM item) {
        checkNotNull(item, "item must be !null");
        return new ApiResponse<>(null, Collections.singletonList(item), null, null);
    }

    @Nonnull
    public static <ITEM> ApiResponse<List<ITEM>> nonexistentPage(@Nonnull Page.PageInfo pageInfo) {
        return new ApiResponse<>(Page.ofNonexistentPage(pageInfo), Collections.emptyList(), null, null);
    }

    @Nonnull
    public static <ITEM> ApiResponse<List<ITEM>> page(@Nonnull Page.PageInfo pageInfo, @Nonnull List<ITEM> pageData) {
        checkNotNull(pageData, "pageData must be !null");
        return new ApiResponse<>(Page.ofPage(pageInfo, pageData), pageData, null, null);
    }

    @Nonnull
    public static <ITEM> ApiResponse<List<ITEM>> emptySimplePage(@Nonnull Page.PageQuery pageQuery) {
        return new ApiResponse<>(Page.ofEmptySimplePage(pageQuery), Collections.emptyList(), null, null);
    }

    @Nonnull
    public static <ITEM> ApiResponse<List<ITEM>> simplePage(@Nonnull Page.PageQuery pageQuery, @Nonnull List<ITEM> pageData) {
        checkNotNull(pageData, "pageData must be !null");
        return new ApiResponse<>(Page.ofSimplePage(pageQuery, pageData), pageData, null, null);
    }

    @Nonnull
    public static <T, ITEM> ApiResponse<T> simplePage(@Nonnull Page.PageQuery pageQuery, @Nonnull List<ITEM> pageData, @Nonnull T result) {
        checkNotNull(pageData, "pageData must be !null");
        checkNotNull(result, "result must be !null");
        return new ApiResponse<>(Page.ofSimplePage(pageQuery, pageData), result, null, null);
    }

    @Nonnull
    public static <T> ApiResponse<T> failure(@Nullable String description) {
        return new ApiResponse<>(null, null, null, description);
    }

    @Nonnull
    public static <T> ApiResponse<T> failure(@Nullable Reason reason) {
        return new ApiResponse<>(null, null, reason != null ? Collections.singletonList(reason) : null, null);
    }

    @Nonnull
    public static <T> ApiResponse<T> failure(@Nullable List<Reason> reasons) {
        return new ApiResponse<>(null, null, (reasons != null && !reasons.isEmpty()) ? reasons : null, null);
    }

    @Nonnull
    public static <T> ApiResponse<T> failure(@Nullable Reason reason, @Nullable String description) {
        return new ApiResponse<>(null, null, reason != null ? Collections.singletonList(reason) : null, description);
    }

    @Nonnull
    public static <T> ApiResponse<T> failure(@Nullable List<Reason> reasons, @Nullable String description) {
        return new ApiResponse<>(null, null, (reasons != null && !reasons.isEmpty()) ? reasons : null, description);
    }

    @Nonnull
    public static <T> ApiResponse<T> failure(@Nonnull Throwable t, @Nullable Reason reason, @Nullable String descriptionPrefix) {
        KeyAware kaMessage = KeyAware.of(t.getMessage());
        if (kaMessage.key != null) {
            //replace key, omit params, ok
            reason = Reason.of(
                kaMessage.key,
                reason != null ? reason.ref : null
            );
        }
        return new ApiResponse<>(
            null,
            null,
            reason != null ? Collections.singletonList(reason) : null,
            descriptionPrefix != null || kaMessage.value != null ?
                (descriptionPrefix != null ? descriptionPrefix : "") + (kaMessage.value != null ? kaMessage.value : "") :
                null
        );
    }

    @JsonCreator
    public ApiResponse(
        @JsonProperty("page") @Nullable Page page,
        @JsonProperty("result") @Nullable T result,
        @JsonProperty("reasons") @Nullable List<Reason> reasons,
        @JsonProperty("description") @Nullable String description
    ) {
        this.page = page;
        this.result = result;
        this.reasons = reasons;
        this.description = description;
    }

    @JsonIgnore
    @Nullable
    @Override
    public String description() {
        return description;
    }

    @JsonIgnore
    @Nullable
    @Override
    public T result() {
        return result;
    }

    @JsonIgnore
    @Nonnull
    public ApiResponse<T> noDescription() {
        return description != null ? new ApiResponse<>(page, result, reasons, null) : this;
    }

    @JsonIgnore
    @Nonnull
    public ApiResponse<T> withAbsent() {
        return new ApiResponse<>(page, null, reasons, description);
    }

    @JsonIgnore
    @Nonnull
    public ApiResponse<T> withResult(@Nonnull T result) {
        checkNotNull(result, "result must be !null");
        return new ApiResponse<>(page, result, reasons, description);
    }

    @JsonIgnore
    @Nonnull
    public ApiResponse<T> withResultOrAbsent(@Nullable T result) {
        return new ApiResponse<>(page, result, reasons, description);
    }

    @Override
    public String toString() {
        return "{" +
            (page != null ? "\"page\": " + page : "") +
            (page != null ? ", " : "") + "\"result\": " + result + //nullable included
            (reasons != null ? ", \"reasons\": " + reasons : "") +
            (description != null ? ", \"description\": " + description : "") +
            "}";
    }
}
