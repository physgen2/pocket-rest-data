package com.freesolutions.pocket.pocketrestdata.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.List;

import static com.freesolutions.pocket.pocketrestdata.utils.Utils.checkArgument;

/**
 * @author Stanislau Mirzayeu
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Immutable
public class Reason {

    @JsonProperty("key")
    @Nonnull
    public final String key;

    @JsonProperty("ref")
    @Nullable
    public final String ref;

    @JsonProperty("params")
    @Nullable
    public final List<Object> params;

    @Nonnull
    public static Reason of(@Nonnull String key) {
        return new Reason(key, null, null);
    }

    @Nonnull
    public static Reason of(@Nonnull String key, @Nullable String ref) {
        return new Reason(key, ref, null);
    }

    @Nonnull
    public static Reason of(@Nonnull String key, @Nullable List<?> params) {
        return new Reason(key, null, params);
    }

    @Nonnull
    public static Reason of(@Nonnull String key, @Nullable String ref, @Nullable List<?> params) {
        return new Reason(key, ref, params);
    }

    @JsonCreator
    public Reason(
        @JsonProperty("key") @Nonnull String key,
        @JsonProperty("ref") @Nullable String ref,
        @JsonProperty("params") @Nullable List<?> params
    ) {
        checkArgument(!key.isEmpty(), "must be !empty");
        checkArgument(ref == null || !ref.isEmpty(), "must be null or !empty");
        @SuppressWarnings("unchecked")
        List<Object> lParams = (List<Object>) params;
        this.key = key;
        this.ref = ref;
        this.params = lParams;
    }

    @Override
    public String toString() {
        return "{" +
            "\"key\": " + key +
            (ref != null ? ", \"ref\": " + ref : "") +
            (params != null ? ", \"params\": " + params : "") +
            "}";
    }
}
