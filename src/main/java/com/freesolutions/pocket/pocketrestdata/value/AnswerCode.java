package com.freesolutions.pocket.pocketrestdata.value;

import com.fasterxml.jackson.annotation.JsonValue;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public enum AnswerCode {

    OK("ok", true, false, false),
    REJECTED("rejected", false, true, false),
    ERROR("error", false, false, true);

    @JsonValue
    public final String value;

    public final boolean ok;
    public final boolean rejected;
    public final boolean error;

    @Nonnull
    public static AnswerCode ofValue(@Nonnull String value) {
        AnswerCode result = ofValueOrNull(value);
        if (result == null) {
            throw new IllegalArgumentException("Can't determine AnswerCode for value: " + value);
        }
        return result;
    }

    @Nullable
    public static AnswerCode ofValueOrNull(@Nonnull String value) {
        for (AnswerCode v : values()) {
            if (v.value.equals(value)) {
                return v;
            }
        }
        return null;
    }

    AnswerCode(@Nonnull String value, boolean ok, boolean rejected, boolean error) {
        this.value = value;
        this.ok = ok;
        this.rejected = rejected;
        this.error = error;
    }
}
