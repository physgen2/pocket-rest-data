package com.freesolutions.pocket.pocketrestdata.value;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;
import java.util.*;
import java.util.function.BiConsumer;

import static com.freesolutions.pocket.pocketrestdata.utils.Utils.checkArgument;

/**
 * @author Stanislau Mirzayeu
 * <p/>
 * - Try to keep order of headers modification, under the hood LinkedHashMap + LinkedList are used.
 * <p/>
 * - Header names are stored with next convention: first char - upper case, rest - lower case.
 * <p/>
 * - Methods already handle letter-case of names, so you can add / get headers by names with any letter-case.
 * <p/>
 * - When comparing names directly (e.g. after taking them explicitly) caller code must handle letter-case by hands.
 */
@NotThreadSafe
public class Headers {

    private static final int DEFAULT_CAPACITY = 32;

    @Nonnull
    private final Map<String, List<String>> data;//actually LinkedHashMap<String, LinkedList<String>>

    @Nonnull
    public static Headers of() {
        return of(DEFAULT_CAPACITY);
    }

    @Nonnull
    public static Headers of(int capacity) {
        return new Headers(new LinkedHashMap<>(capacity));
    }

    @Nonnull
    public static Headers of(@Nonnull Map<? extends String, ? extends List<? extends String>> other) {
        return new Headers(deepCopyWithChecks(other));
    }

    @Nonnull
    public static Headers of(@Nonnull Headers headers) {
        return new Headers(deepCopyWithoutChecks(headers.data));
    }

    @Nonnull
    public Set<String> getNames() {
        return Collections.unmodifiableSet(data.keySet());
    }

    public boolean hasHeader(@Nonnull String name) {
        return data.containsKey(nn(name));
    }

    public boolean hasHeader(@Nonnull String name, @Nonnull String value) {
        List<String> values = data.get(nn(name));
        return values != null && values.contains(value);
    }

    @Nonnull
    public List<String> getHeaders(@Nonnull String name) {
        List<String> values = data.get(nn(name));
        return values != null ?
            Collections.unmodifiableList(values) :
            Collections.emptyList();
    }

    @Nullable
    public String findFirst(@Nonnull String name) {
        List<String> values = data.get(nn(name));
        return values != null && !values.isEmpty() ? values.get(0) : null;
    }

    public void clear() {
        data.clear();
    }

    @Nonnull
    public Headers addHeader(@Nonnull String name, @Nonnull String value) {
        checkValue(value);
        data.computeIfAbsent(nn(name), ignored -> new LinkedList<>()).add(value);
        return this;
    }

    @Nonnull
    public Headers addHeaders(@Nonnull Headers headers) {
        headers.data.forEach((name, values) ->
            data.computeIfAbsent(name, ignored -> new LinkedList<>()).addAll(values)
        );
        return this;
    }

    @Nonnull
    public List<String> setHeader(@Nonnull String name, @Nonnull String value) {
        checkValue(value);
        List<String> newValues = new LinkedList<>();
        newValues.add(value);
        List<String> oldValues = data.put(nn(name), newValues);
        return oldValues != null ? oldValues : Collections.emptyList();
    }

    public boolean removeOneHeader(@Nonnull String name, @Nonnull String value, boolean firstNLast) {
        name = nn(name);
        boolean removed = false;
        List<String> values = data.get(name);
        if (values != null) {
            ListIterator<String> i = values.listIterator(firstNLast ? 0 : value.length());
            while (firstNLast ? i.hasNext() : i.hasPrevious()) {
                String x = firstNLast ? i.next() : i.previous();
                if (x.equals(value)) {
                    i.remove();
                    removed = true;
                    break;
                }
            }
            if (removed && values.isEmpty()) {
                data.remove(name);//remove empty sub-list
            }
        }
        return removed;
    }

    public boolean removeHeaders(@Nonnull String name, @Nonnull String value) {
        name = nn(name);
        List<String> values = data.get(name);
        if (values == null) {
            return false;
        }
        if (!values.removeIf(x -> x.equals(value))) {
            return false;
        }
        if (values.isEmpty()) {
            data.remove(name);//remove empty sub-list
        }
        return true;
    }

    @Nonnull
    public List<String> removeHeaders(@Nonnull String name) {
        List<String> oldValues = data.remove(nn(name));
        return oldValues != null ? oldValues : Collections.emptyList();
    }

    @Nonnull
    public List<Map.Entry<String, String>> copyOfEntries() {
        List<Map.Entry<String, String>> result = new ArrayList<>();
        data.forEach((name, values) ->
            values.forEach(value ->
                result.add(new AbstractMap.SimpleImmutableEntry<>(name, value))
            )
        );
        return result;
    }

    public void forEach(@Nonnull BiConsumer<? super String, ? super String> action) {
        data.forEach((name, values) -> values.forEach(value -> action.accept(name, value)));
    }

    public void forEachName(@Nonnull BiConsumer<? super String, ? super List<String>> action) {
        data.forEach((name, values) -> action.accept(name, Collections.unmodifiableList(values)));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Headers)) return false;
        Headers other = (Headers) o;
        return data.equals(other.data);
    }

    @Override
    public int hashCode() {
        return data.hashCode();
    }

    @Override
    public String toString() {
        return data.toString();
    }

    private Headers(@Nonnull Map<String, List<String>> data) {
        this.data = data;
    }

    @Nonnull
    private static Map<String, List<String>> deepCopyWithChecks(@Nonnull Map<? extends String, ? extends List<? extends String>> source) {
        Map<String, List<String>> result = new LinkedHashMap<>();
        source.forEach((name, values) -> {
            values.forEach(Headers::checkValue);
            result.put(nn(name), new LinkedList<>(values));
        });
        return result;
    }

    @Nonnull
    private static Map<String, List<String>> deepCopyWithoutChecks(@Nonnull Map<? extends String, ? extends List<? extends String>> source) {
        Map<String, List<String>> result = new LinkedHashMap<>();
        source.forEach((name, values) -> result.put(name, new LinkedList<>(values)));
        return result;
    }

    @Nonnull
    private static String nn(@Nonnull String name) {

        //short case
        if (name.isEmpty()) {
            return name;
        }

        char[] chars = null;//initially try to not touch original name

        //first char
        char ch = name.charAt(0);
        if (ch >= 'a' && ch <= 'z') {
            chars = name.toCharArray();//first time
            chars[0] = (char) (ch - ('a' - 'A'));
        } else {
            checkArgument(ch != '\r' && ch != '\n', "Invalid chars are present in header name: %s", name);
        }

        //rest chars
        int l = name.length();
        for (int i = 1; i < l; i++) {
            ch = name.charAt(i);
            if (ch >= 'A' && ch <= 'Z') {
                if (chars == null) {
                    chars = name.toCharArray();
                }
                chars[i] = (char) (ch + ('a' - 'A'));
            } else {
                checkArgument(ch != '\r' && ch != '\n', "Invalid chars are present in header name: %s", name);
            }
        }

        return chars != null ? new String(chars) : name;
    }

    private static void checkValue(@Nonnull String value) {
        int l = value.length();
        for (int i = 0; i < l; i++) {
            char ch = value.charAt(i);
            if (ch == '\r') {
                checkArgument(i < l - 2, "Invalid CR in header value: %s", value);
                char ch1 = value.charAt(i + 1);
                char ch2 = value.charAt(i + 2);
                checkArgument(ch1 == '\n', "Invalid char after CR in header value: %s", value);
                checkArgument(ch2 == ' ' || ch2 == '\t', "No whitespace after CRLF in header value: %s", value);
                i += 2;
            } else {
                checkArgument(ch != '\n', "Invalid LF in header value");
            }
        }
    }
}
