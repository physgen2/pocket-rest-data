package com.freesolutions.pocket.pocketrestdata.value;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import static com.freesolutions.pocket.pocketrestdata.utils.Utils.checkArgument;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class KeyAware {

    @Nullable
    public final String key;
    @Nullable
    public final String value;

    @Nonnull
    public static KeyAware of(@Nullable String value) {
        if (value == null) {
            return new KeyAware(null, null);
        }
        value = value.strip();
        if (value.length() < 3) {
            return new KeyAware(null, value);
        }
        if (value.charAt(0) == '#') {
            int idx = value.indexOf(';');
            if (idx > 1) {
                String key = value.substring(1, idx).strip();
                if (!key.isEmpty()) {
                    String description = value.substring(idx + 1).strip();
                    return new KeyAware(key, !description.isEmpty() ? description : null);
                }
            }
        }
        return new KeyAware(null, value);
    }

    @Nonnull
    public String keyOrDefault(@Nonnull String defaultKey) {
        return key != null ? key : defaultKey;
    }

    @Override
    public String toString() {
        return "KeyAware[key: " + key + ", value: " + value + "]";
    }

    private KeyAware(@Nullable String key, @Nullable String value) {
        checkArgument(key == null || !key.isEmpty(), "key must be null || !empty");
        this.key = key;
        this.value = value;
    }
}
