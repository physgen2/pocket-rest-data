package com.freesolutions.pocket.pocketrestdata.value;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public enum HttpMethod {

    HEAD("HEAD"), //NOTE: not related to REST

    POST("POST"),
    GET("GET"),
    PUT("PUT"),
    PATCH("PATCH"),
    DELETE("DELETE");

    @Nonnull
    public static HttpMethod ofHttpName(@Nonnull String httpName) {
        for (HttpMethod value : values()) {
            if (value.httpName.equals(httpName)) {
                return value;
            }
        }
        throw new IllegalArgumentException("Unknown httpName: " + httpName);
    }

    @Nonnull
    public final String httpName;

    HttpMethod(@Nonnull String httpName) {
        this.httpName = httpName;
    }
}
