package com.freesolutions.pocket.pocketrestdata.utils;

import org.junit.jupiter.api.Test;

import java.util.Random;

import static com.freesolutions.pocket.pocketrestdata.utils.Utils.checkArgument;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Stanislau Mirzayeu
 */
public class UriEncoderTest {

    private static final Random RANDOM = new Random();

    private static final char[] CHARS;

    static {
        int asciiCount = 256;
        int rusCount = 33;
        char[] chars = new char[asciiCount + 2 * rusCount];
        for (int i = 0; i < asciiCount; i++) {
            chars[i] = (char) i;
        }
        for (int i = 0; i < rusCount; i++) {
            chars[i] = (char) ('а' + i);//small russian 'а'
        }
        for (int i = 0; i < rusCount; i++) {
            chars[i] = (char) ('А' + i);//large russian 'А'
        }
        CHARS = chars;
    }

    @Test
    public void encodeDecodeCycles() {
        int n = 1_000_000;
        for (int i = 0; i < n; i++) {
            String value = randomString(20);
            String encoded = UriEncoder.encode(value);
            String decoded = UriEncoder.decode(encoded);
            assertEquals(value, decoded);
        }
    }

    private static String randomString(int n) {
        checkArgument(n >= 0);
        if (n == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; i++) {
            sb.append(CHARS[RANDOM.nextInt(CHARS.length)]);
        }
        return sb.toString();
    }
}
